g_RNG_seed = 1234567890;#1234567890;#1337;

import warnings

warnings.filterwarnings('ignore');
warnings.filterwarnings("ignore", category=DeprecationWarning);

import random

random.seed(g_RNG_seed);

import numpy as np

np.random.seed(g_RNG_seed);
np.set_printoptions(threshold=np.nan);

import matplotlib.pyplot as plt
from Bio.PDB import *
from Bio import SeqIO
import sys
import csv
import pandas as pd
import dssp_parse as dd
from sklearn.utils import shuffle
from sklearn import cross_validation, preprocessing
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Embedding, Input, merge
from keras.layers import Convolution1D, MaxPooling1D
from keras import backend as K
from xgboost.sklearn import XGBRegressor, XGBClassifier
from sklearn.metrics import mean_squared_error, accuracy_score, f1_score, confusion_matrix, r2_score
from scipy.stats.stats import pearsonr
#from imblearn.over_sampling import RandomOverSampler, SMOTE
#from oversampling import random_os
#from smote import sample

#------------------------------------------------------------------------------
def correct_mut(data):
	corrs = {
		("1DVF", "D:I101A") : "D:I97A",
		("1DVF", "D:N55A")  : "D:N54A",
		("1DVF", "D:Q104A") : "D:Q100A",
		("1DVF", "D:R106A") : "D:R100bA",
		("3NPS", "A:D46A")  : "A:D60aA",
		("3NPS", "A:D47A")  : "A:D60bA",
		("3NPS", "A:D91A")  : "A:D96A",
		("3NPS", "A:D214A") : "A:D217A",
		("3NPS", "A:E163A") : "A:E169A",
		("3NPS", "A:F50A")  : "A:F60eA",
		("3NPS", "A:F89A")  : "A:F94A",
		("3NPS", "A:F92A")  : "A:F97A",
		("3NPS", "A:H138A") : "A:H143A",
		("3NPS", "A:I26A")  : "A:I41A",
		("3NPS", "A:I45A")  : "A:I60A",
		("3NPS", "A:K221A") : "A:K224A",
		("3NPS", "A:L147A") : "A:L153A",
		("3NPS", "A:N90A")  : "A:N95A",
		("3NPS", "A:Q140A") : "A:Q145A",
		("3NPS", "A:Q168A") : "A:Q174A",
		("3NPS", "A:Q169A") : "A:Q175A",
		("3NPS", "A:Q218A") : "A:Q221aA",
		("3NPS", "A:Q23A")  : "A:Q38A",
		("3NPS", "A:R219A") : "A:R222A",
		("3NPS", "A:R48A")  : "A:R60cA",
		("3NPS", "A:R51A")  : "A:R60fA",
		("3NPS", "A:R82A")  : "A:R87A",
		("3NPS", "A:T144A") : "A:T150A",
		("3NPS", "A:T93A")  : "A:T98A",
		("3NPS", "A:Y141A") : "A:Y146A",
		("3NPS", "A:Y52A")  : "A:Y60gA",
		("1N8Z", "H:N54A")  : "H:N55A",
		("1N8Z", "H:T53A")  : "H:T54A",
		("1N8Z", "H:R58A")  : "H:R59A",
		("1N8Z", "H:D98A")  : "H:D102A",
		("1N8Z", "H:D98W")  : "H:D102W",
		("1N8Z", "H:Y56A")  : "H:Y57A",
		("1N8Z", "H:F100A") : "H:F104A"
	};
	newdata = data;
	for index, mut in enumerate(data["Mutation"]):
		pdb = data["#PDB"][index];
		newmut = mut;
		for key, value in corrs.iteritems():
			if pdb==key[0] and mut==key[1]:
				newmut = value;
		newdata["Mutation"][index] = newmut;
	return newdata;

#------------------------------------------------------------------------------
def read_data():
	data = {};
	with open("data/abdata.csv", "rU") as f:
		reader = csv.reader(f);
		headers = reader.next();
		data = {};
		for h in headers:
			data[h] = [];
		for row in reader:
			rmv1n8z = False;
			if(row[0]=="1N8Z" and (any(c.islower() for c in row[4]) or row[4]=="H:Y102V")):
				rmv1n8z = True;
			if("," not in row[4] and row[5]!="8" and "HM" not in row[0] and not rmv1n8z):
				for h, v in zip(headers, row):
					data[h].append(v);
	data = correct_mut(data);
	return data;

#------------------------------------------------------------------------------
def get_pdbs(data):
	return sorted(set(data["#PDB"]), key=data["#PDB"].index);

#------------------------------------------------------------------------------
def ab_list():
	notab = ["3K2M", "1KTZ", "1AK4", "1JTG", "1FFW", "1DVF"];
	pdbs = get_pdbs(read_data())
	ab = [x for x in pdbs if x not in notab];
	ab.append("2OSL");
	return ab;

#------------------------------------------------------------------------------
def parse_cdr():
	with open("data/cdrdata.txt") as f:
		lines = f.readlines();
	mat = [];
	for line in lines:
		mat.append(line.split());
	return np.array(mat);

#------------------------------------------------------------------------------
def get_pdb_seq(pdb, ch):
	seq = "";
	for record in SeqIO.parse("data/pdbseq/"+pdb+ch+".fasta", "fasta"):
		seq = str(record.seq);
	return seq;

#------------------------------------------------------------------------------
def cdr_seq(pdbs):
	cdrdata = parse_cdr();
	cdrdict = {};
	cdrch = {};
	ablist = ab_list();
	for pdb in ablist:
		for i in range(2, 23521):
			entry = cdrdata[i];
			if entry[2] == pdb:
				pch = entry[2]+entry[3];
				horl = entry[4][0];
				seq = get_pdb_seq(entry[2], entry[3]);
				cdrind = seq.index(entry[10]);
				if pch in cdrdict:
					cdrdict[pch].extend([cdrind, cdrind+len(entry[10])]);
				else:
					cdrdict.update({pch: []});
					cdrdict[pch].extend([cdrind, cdrind+len(entry[10])]);
					cdrch.update({pch: horl});
	return (cdrdict, cdrch);

#------------------------------------------------------------------------------
def parse_PDB(pdb):
	parser = PDBParser();
	structure = parser.get_structure(pdb, "data/pdbfiles/pdb"+pdb.lower()+".ent");
	return structure;

#------------------------------------------------------------------------------
def get_mut(pdb, data):
	rows = np.where(np.array(data["#PDB"])==pdb);
	muts = [];
	for r in rows[0].tolist():
		muts.append(data["Mutation"][r]);
	return muts;

#------------------------------------------------------------------------------
def chain_mapping(pdb, mut):
	chmap = {
		"1DQJL": "A", "1DQJH": "B", "1MLCL": "A", "1MLCH": "D",
		"1N8ZL": "A", "1N8ZH": "B", "1VFBL": "A", "1VFBH": "B",
		"1YY9L": "C", "1YY9H": "D", "2NYYL": "C", "2NYYH": "D",
		"3BN9L": "E", "3BN9H": "F", "3NPSH": "B", "3NPSL": "C"
	};
	if pdb+mut[0] in chmap:
		chmut = chmap[pdb+mut[0]]+mut[1:len(mut)]+mut[0];
	else:
		chmut = mut+"X";
	return chmut;

#------------------------------------------------------------------------------
def parse_mut(pdb, mut):
	mut = chain_mapping(pdb, mut);
	wild_type = mut[2];
	mutant = mut[len(mut)-2];
	chain = mut[0];
	resid = [" ", " ", " "];
	if not mut[len(mut)-3].isdigit():
		resid[1] = int(mut[3:len(mut)-3]);
		resid[2] = mut[len(mut)-3].upper();
	else:
		resid[1] = int(mut[3:len(mut)-2]);
	return (wild_type, mutant, chain, tuple(resid), mut);

#------------------------------------------------------------------------------
def AAbrev(aacid):
	d = {
		"CYS": "C", "ASP": "D", "SER": "S", "GLN": "Q", "LYS": "K",
		"ILE": "I", "PRO": "P", "THR": "T", "PHE": "F", "ASN": "N",
		"GLY": "G", "HIS": "H", "LEU": "L", "ARG": "R", "TRP": "W",
		"ALA": "A", "VAL": "V", "GLU": "E", "TYR": "Y", "MET": "M"
	};
	ans = d[aacid] if d[aacid] else -1
	return ans;

#------------------------------------------------------------------------------
def aavector(pdb, data, rows):
	featvec = [];
	aacids = "ARNDCQEGHILKMFPSTWYV";
	aacids = sorted(aacids);
	for mut in get_mut(pdb, data):
		aavector_vec = [0]*20;
		(wild_type, mutant, ch, resid, mut) = parse_mut(pdb, mut);
		aavector_vec[aacids.index(wild_type)] = -1;
		aavector_vec[aacids.index(mutant)] = 1;
		featvec.append(aavector_vec);
	features = [];
	for i in range(1, 21):
		features.append("AA"+str(i));
	return (featvec, features);

#------------------------------------------------------------------------------
def atchley():
	f1 = [
		-0.591, -1.343,  1.050, 1.357, -1.006, -0.384, 0.336, -1.239,
		 1.831, -1.019, -0.663, 0.945,  0.189,  0.931, 1.538, -0.228,
		-0.032, -1.337, -0.595, 0.260
	];
	f2 = [
		-1.302,  0.465,  0.302, -1.453, -0.590,  1.652, -0.417, -0.547,
		-0.561, -0.987, -1.524,  0.828,  2.081, -0.179, -0.055,  1.399,
		 0.326, -0.279,  0.009,  0.830
	];
	f3 = [
		-0.733, -0.862, -3.656, 1.477,  1.891,  1.330, -1.673,  2.131,
		 0.533, -1.505,  2.219, 1.299, -1.628, -3.005,  1.502, -4.760,
		 2.213, -0.544,  0.672, 3.097
	];
	f4 = [
		 1.570, -1.020, -0.259,  0.113, -0.397,  1.045, -1.474, 0.393,
		-0.277,  1.266, -1.005, -0.169,  0.421, -0.503,  0.440, 0.670,
		 0.908,  1.242, -2.128, -0.838
	];
	f5 = [
		-0.146, -0.255, -3.242, -0.837,  0.412,  2.064, -0.078,  0.816,
		 1.648, -0.912,  1.212,  0.933, -1.392, -1.853,  2.897, -2.647,
		 1.313, -1.262, -0.184,  1.512
	];
	properties = [f1, f2, f3, f4, f5];
	featurelist = ["f1", "f2", "f3", "f4", "f5"];
	return(properties, featurelist);

#------------------------------------------------------------------------------
def aachange(pdb, data, rows):
	features = [];
	featvec = [];
	(properties, featurelist) = atchley();
	for r in rows[0].tolist():
		fvec = [];
		mut = data["Mutation"][r];
		(wild_type, mutant, ch, resid, mut) = parse_mut(pdb, mut);
		aacids = "ACDEFGHIKLMNPQRSTVWY";
		wild_ind = aacids.index(wild_type);
		mut_ind = aacids.index(mutant);
		for index, feat in enumerate(properties):
			fvec.extend([feat[wild_ind], feat[mut_ind]]);
		featvec.append(fvec);
	for feat in featurelist:
		features.extend([feat+"_wt", feat+"_mut"]);
	return (featvec, features, properties, featurelist);

#------------------------------------------------------------------------------
def rmv_het(residues):
	newres = [];
	for res in residues:
		if res.id[0] == " ":
			newres.append(res);
	return newres;

#------------------------------------------------------------------------------
def residues_to_seq(residues):
	seq = [];
	for res in residues:
		seq.append(AAbrev(res.get_resname()));
	return "".join(seq);

#------------------------------------------------------------------------------
def AAexp(aacid):
	d = {
		"CYS": "C", "ASP": "D", "SER": "S", "GLN": "Q", "LYS": "K",
		"ILE": "I", "PRO": "P", "THR": "T", "PHE": "F", "ASN": "N",
		"GLY": "G", "HIS": "H", "LEU": "L", "ARG": "R", "TRP": "W",
		"ALA": "A", "VAL": "V", "GLU": "E", "TYR": "Y", "MET": "M"
	};
	d = dict((v, k) for k, v in d.items())
	return d[aacid]

#------------------------------------------------------------------------------
def add_missing(resseq, residues):
	atomseq = residues_to_seq(residues)

	begseq = atomseq[0:10]
	index = resseq.index(begseq)

	firstres = residues[0]
	firstind = firstres.id[1]

	for x in range(1, index + 1):
		resid = (' ', firstind - x, ' ')
		resname = AAexp(resseq[index - x])
		newres = Residue.Residue(resid, resname, '    ')
		residues.insert(0, newres)

	for x in range(0, len(residues) - 1):
		ind1 = residues[x].id[1]
		ind2 = residues[x+1].id[1]

		if(ind2 - ind1 > 1 and not((ind1 == 91 and ind2 == 96) or (ind1 == 30 and ind2 == 33))):
			for i in range(ind2 - ind1-1, 0, -1):
				resid = (' ', ind1 + i, ' ')
				resname = AAexp(resseq[x + i])
				newres = Residue.Residue(resid, resname, '    ')
				residues.insert(x+1, newres)

	dlength = len(resseq) - len(residues)
	maxindex = residues[len(residues) - 1].id[1]
	origlen = len(residues)

	for x in range(len(residues), len(residues) + dlength):
		resid = (' ', maxindex+(x - origlen)+1, ' ')
		resname = AAexp(resseq[x])
		newres = Residue.Residue(resid, resname, '    ')
		residues.append(newres)

	return residues

#------------------------------------------------------------------------------
def PDB_chain(pdb, struct, ch):
	chain = struct[0][ch];
	residues = chain.get_residues();
	residues = rmv_het(list(residues));
	resseq = str(SeqIO.read(open("data/pdbseq/"+pdb+ch+".fasta"), "fasta").seq);
	residues = add_missing(resseq, residues);
	return (chain, residues);

#------------------------------------------------------------------------------
def conv_index(pdb, struct, mut):
	(wild_type, mutant, ch, resid, mut) = parse_mut(pdb, mut);
	(chain, residues) = PDB_chain(pdb, struct, ch);
	resids = [];
	for res in residues:
		resids.append(res.id);
	return resids.index(resid);

#------------------------------------------------------------------------------
def extract_pssm(fname):
	pssm = [];
	info = [];
	with open(fname, "r") as f:
		plaintext = f.readlines();
	for x in range(3, 10000):
		line = plaintext[x].split();
		freq = [];
		if not line: # not ideal
			break;
		for i in range(2, 22):
			freq.append(int(line[i]));
		info.append(float(line[len(line)-2]));
		pssm.append(freq);
	pssm = np.array(pssm);
	return (pssm, info); # "info" isn't used

#------------------------------------------------------------------------------
def get_profile(pssm, index): # probably don't need this function
	return list(pssm[index]);

#------------------------------------------------------------------------------
def get_evolfeatures(pdb, data, rows, struct):
	featvec = [];
	aacids = "ARNDCQEGHILKMFPSTWYV";
	for mut in get_mut(pdb, data):
		fvec = [];
		(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
		(pssm, info) = extract_pssm("data/cspssms/"+pdb.lower()+ch+"pssm.matrix");
		normindex = conv_index(pdb, struct, mut);
		fvec.extend(get_profile(pssm, normindex));
		featvec.append(fvec);
	features = [];
	features.append("mut_info");
	for i in range(1, 21):
		features.append("Mut_profile"+str(i));
	return (featvec, features);

#------------------------------------------------------------------------------
def parse_ipatch(pdb, struct):
	validlist = [
		"1DQJ", "1MHP", "1MLC", "1N8Z", "1VFB", "1YY9", "2NYY",
		"3BDY", "3BE1", "3BN9", "3HFM", "3NGB", "2OSL"
	];
	if not pdb in validlist:
		return -1;
	else:
		parser = PDBParser();
		structure = parser.get_structure("name", "data/ipatch/ip"+pdb.lower()+".pdb");
		model = structure[0];
		chip = {};
		for cha in model.get_chains():
			ch = cha.id;
			eachip = {};
			chain = model[ch];
			residues = chain.get_residues();
			for res in residues:
				iscore = res.get_unpacked_list()[0].get_bfactor();
				eachip.update({res.id: iscore});
			chip.update({ch: eachip});
		return chip;

#------------------------------------------------------------------------------
def parse_stab(fname):
	with open(fname, "r") as f:
		line = f.readline();
	stats = line.split();
	scores = map(float, stats[2:len(stats)-1]);
	return scores;

#------------------------------------------------------------------------------
def ss_to_num(ss):
	sstruc = "HBEGITS ";
	return sstruc.index(ss)+1;

#------------------------------------------------------------------------------
def sec_strucmut(pdb, mut, dd_ob):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	chains  = dd_ob.getChain();
	resinds = dd_ob.getResnums();
	sstruc  = dd_ob.getSecStruc();
	inscode = dd_ob.getInsCode();

	inds = np.where(np.array(chains)==ch)[0];
	resind = str(resid[1])+resid[2] if not resid[2]==" " else str(resid[1]);

	ss = "";
	for ind in inds:
		if resinds[ind]+inscode[ind] == resind:
			ss = sstruc[ind];
			break;
	return ss_to_num(ss[2]);

#------------------------------------------------------------------------------
def parse_rd(pdb):
	with open("data/rdfiles/"+pdb+".pdb-residue.depth.txt", "r") as f:
		lines = f.readlines();
	mat = [];
	for line in lines:
		mat.append(line.split());
	return np.array(mat);

#------------------------------------------------------------------------------
def get_rd(pdb, mut):
	rdmat = parse_rd(pdb);
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	rdid = ch+":"+(str(resid[1])+resid[2] if not resid[2]==" " else str(resid[1]));
	rdmut = -1;
	for arr in list(rdmat):
		if arr[0] == rdid:
			rdmut = float(arr[2]);
			break;
	return rdmut;

#------------------------------------------------------------------------------
def get_cdr(pdb, struct, mut, cdrdict):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	normindex = conv_index(pdb, struct, mut);
	ans = -999;
	key = pdb+ch;
	if key in cdrdict:
		cdrind = cdrdict[key];
		ans = 1;
		for i in [0, 2, 4]: # CDR 1, 2, or 3
			if normindex>=cdrind[i] and normindex<cdrind[i+1]:
				ans = 2;
				break;
	return ans;

#------------------------------------------------------------------------------
def get_iscore(pdb, mut, chip):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	iscore = -999;
	if not chip==-1 and ch in chip:
		reslist = chip[ch];
		iscore = reslist[resid];
	return iscore;

#------------------------------------------------------------------------------
def foldx_mut(pdb, mut, pdbsc):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	resfull = str(resid[1])+resid[2].lower() if not resid[2]==" " else str(resid[1]);
	mut_scores = parse_stab("data/stab/"+pdb.lower()+ch+wild_type+resfull+mutant+"stab_ST.fxout");
	diff = [i-j for i, j in zip(pdbsc, mut_scores)];
	return diff;

#------------------------------------------------------------------------------
def get_structfeatures(pdb, data, rows, struct, cdrdict):
	featvec = [];
	dd_ob = dd.DSSPData();
	dd_ob.parseDSSP("data/dsspfiles/dssp"+pdb.lower());
	chip = parse_ipatch(pdb, struct);
	pdbsc = parse_stab("data/stab/"+pdb.lower()+"stab_ST.fxout");
	features = [];
	for mut in get_mut(pdb, data):
		fvec = [];

		ss = sec_strucmut(pdb, mut, dd_ob);
		fvec.append(ss);

		rd = get_rd(pdb, mut);
		fvec.append(rd);

		cdr = get_cdr(pdb, struct, mut, cdrdict);
		fvec.append(cdr);

		ipatch = get_iscore(pdb, mut, chip);
		fvec.append(ipatch);

		folddiff = foldx_mut(pdb, mut, pdbsc);
		fvec.extend(folddiff);
		fvec.append(folddiff[4]);

		featvec.append(fvec);
	features.extend(["SS", "RD", "CDR", "IP"]);
	for i in range(1, 23):
		features.append("foldx"+str(i));
	return (featvec, features);

#------------------------------------------------------------------------------
def surr_seq(pdb, mut, struct, backward):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	seq = get_pdb_seq(pdb, ch);
	normind = conv_index(pdb, struct, mut);
	fseq = [];
	if backward:
		for i in range(normind+1, len(seq)):
			fseq.append(seq[i]);
		fseq.reverse();
	else:
		for i in range(0, normind):
			fseq.append(seq[i]);
	fseq = "".join(fseq);
	return (fseq, normind, seq);

#------------------------------------------------------------------------------
def AAtonum(aa):
	aacids = "ARNDCQEGHILKMFPSTWYV";
	aacids = sorted(aacids);
	return aacids.index(aa)+1;

#------------------------------------------------------------------------------
def get_window(pdb, mut, struct, w_size):
	(fseq, normindex, seq) = surr_seq(pdb, mut, struct, False);
	(bseq, normindex, seq) = surr_seq(pdb, mut, struct, True);
	bseq = bseq[::-1];
	each_side = int(w_size/2);
	before = after = each_side;
	if len(fseq) < each_side:
		before = len(fseq);
		after = after + (each_side-len(fseq));
	elif len(bseq) < each_side:
		after = len(bseq);
		before = before + (each_side-len(bseq));
	window = [];
	for i in range(len(fseq)-before, len(fseq)):
		window.append(AAtonum(fseq[i]));
	window.append(AAtonum(seq[normindex]));
	for i in range(0, after):
		window.append(AAtonum(bseq[i]));
	return (window, before, after);

#------------------------------------------------------------------------------
def cnn_features(data, w_size):
	pdbs = get_pdbs(data);
	training = [];
	for pdb in pdbs:
		print(pdb);
		struct = parse_PDB(pdb);
		for mut in get_mut(pdb, data):
			(window, fseq, bseq) = get_window(pdb, mut, struct, w_size);
			training.append(window);
	return np.array(training);

#------------------------------------------------------------------------------
def rd_env(pdb, mut, struct, w_size):
	each_side = int(w_size/2);
	rdmat = parse_rd(pdb);
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
	rdid = ch+":"+(str(resid[1])+resid[2] if not resid[2]==" " else str(resid[1]));
	rdenv = [];
	for index, arr in enumerate(list(rdmat)):
		if arr[0] == rdid:
			(window, before, after) = get_window(pdb, mut, struct, w_size);
			for i in range(1, before+1):
				if index-i<0 or not arr[0][0]==rdmat[index-i][0][0]:
					after = after + 1;
				else:
					rdenv.append(float(rdmat[index-i][2]));
			rdenv.reverse();
			rdenv.append(float(arr[2]));
			exbefore = 0;
			for i in range(1, after+1):
				if index+i>=len(list(rdmat)) or not arr[0][0]==rdmat[index+i][0][0]:
					exbefore = exbefore + 1;
				else:
					rdenv.append(float(rdmat[index+i][2]));
			extra = [];
			for i in range(before+1, before+exbefore+1):
				extra.append(float(rdmat[index-i][2]));
			extra.reverse();
			rdenv = extra + rdenv;
	return rdenv;

#------------------------------------------------------------------------------
def sec_env(pdb, mut, struct, dd_ob, w_size):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut)
	chains = dd_ob.getChain()
	resinds = dd_ob.getResnums()
	sstruc = dd_ob.getSecStruc()
	inscode = dd_ob.getInsCode()
	inds = np.where(np.array(chains)==ch)[0]
	resind = str(resid[1]) + resid[2] if not resid[2] == ' ' else str(resid[1])
	secenv = []
	for index, ind in enumerate(inds):
		if(resinds[ind] + inscode[ind] == resind):
			(window, before, after) = get_window(pdb, mut, struct, w_size)
			for i in range(1, before+1):
				if(index - i < 0):
					after = after + 1
				else:
					secenv.append(ss_to_num(sstruc[inds[index - i]][2]))
			secenv.reverse()
			secenv.append(ss_to_num(sstruc[ind][2]))
			exbefore = 0
			for i in range(1, after+1):
				if(index + i >= len(inds)):
					exbefore = exbefore + 1
				else:
					secenv.append(ss_to_num(sstruc[inds[index+i]][2]))
			extra = []
			for i in range(before + 1, before + exbefore + 1):
				extra.append(ss_to_num(sstruc[inds[index - i]][2]))
			extra.reverse()
			secenv = extra + secenv
	return secenv

#------------------------------------------------------------------------------
def ev_env(pdb, mut, struct, w_size):
	(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut)
	fname = './data/cspssms/' + pdb.lower() + ch + 'pssm.matrix'
	(pssm, info) = extract_pssm(fname)
	normindex = conv_index(pdb, struct, mut)
	(window, before, after) = get_window(pdb, mut, struct, w_size)
	aacids = 'ARNDCQEGHILKMFPSTWYV'
	fvec = []
	for i in range(1, before + 1):
		fvec.append(get_profile(pssm, normindex - i))
	fvec.reverse()
	fvec.append(get_profile(pssm, normindex))
	for i in range(1, after + 1):
		fvec.append(get_profile(pssm, normindex + i))
	arr = np.swapaxes(np.array(fvec), 0, 1)
	return arr.tolist()

#------------------------------------------------------------------------------
def ext_features(data, w_size):
	pdbs = get_pdbs(data);
	training = [];
	for pdb in pdbs:
		print(pdb);
		dd_ob = dd.DSSPData();
		dd_ob.parseDSSP("data/dsspfiles/dssp"+pdb.lower());
		struct = parse_PDB(pdb);
		for mut in get_mut(pdb, data):
			fvec = [];
			rdenv = rd_env(pdb, mut, struct, w_size);
			fvec.append(rdenv);
			#-----------------
			secenv = sec_env(pdb, mut, struct, dd_ob, w_size);
			fvec.append(secenv);
			evenv = ev_env(pdb, mut, struct, w_size);
			fvec.extend(evenv);
			#-----------------
			fvec = np.swapaxes(np.array(fvec), 0, 1).tolist();
			training.append(fvec)
	train = np.array(training);
	return train;

#------------------------------------------------------------------------------
def standardize_data(training):
	scaler = preprocessing.StandardScaler().fit(training);
	training = scaler.transform(training);
	return (training, scaler);

#------------------------------------------------------------------------------
def feat_importance(xgbmodel, features):
	mapFeat = dict(zip(["f"+str(i) for i in range(len(features))], features))
	scores = xgbmodel.booster().get_fscore()

	# create panda object containing fscores data
	ts = pd.Series(scores)
	ts.index = ts.reset_index()['index'].map(mapFeat)	
	return ts

#------------------------------------------------------------------------------
def new_model(w_size, f_size):
	seq_input = Input(shape=(w_size+1,), name="seq_input", dtype="int32");
	embed     = Embedding(21, 64, input_length=w_size+1)(seq_input);
	#----------
	str_input = Input(shape=(w_size+1,22), name="str_input", dtype="float32");
	#str_input = Input(shape=(w_size+1,1), name="str_input", dtype="float32");
	#----------
	x         = merge([embed, str_input], mode="concat");

	conv1d = Convolution1D(
		nb_filter=64, filter_length=f_size, border_mode='same',
		activation='relu', subsample_length=1
	)(x);

	flat   = Flatten()(conv1d);
	dense1 = Dense(100, activation='relu')(flat);
	fdense = Dense(1, name='fdense')(dense1);
	model  = Model(input=[seq_input, str_input], output = [fdense]);

	model.compile(optimizer='rmsprop', loss='mean_squared_error');
	return model;

#------------------------------------------------------------------------------
def fit_cnnmod(training, labels, ext_train, w_size, f_size):
	model = new_model(w_size, f_size);
	model.fit([training, ext_train], labels, nb_epoch=10, batch_size=20, verbose=2);
	return model;

#------------------------------------------------------------------------------
def feat_theano(model, tex, tex_ext, w_size, lphase):
	get_output = K.function([model.layers[0].input, model.layers[2].input, K.learning_phase()],[model.layers[4].output]);
	feat = np.array(get_output([tex, tex_ext, lphase]))[0];
	featvec = [];
	index = int(w_size/2);
	for x in range(0, len(tex)):
		fvec = feat[x][index];
		featvec.append(fvec);
	return np.array(featvec);

#------------------------------------------------------------------------------
def mut1dcnn_train_feat(training, ext_train, labels, w_size, f_size):
	(training, scaler) = standardize_data(training);
	exscaler = [];
	for num in range(0, ext_train.shape[2]):
		(ext_train[:, :, num], scale) = standardize_data(ext_train[:, :, num]);
		exscaler.append(scale);
	
	model = fit_cnnmod(training, labels, ext_train, w_size, f_size);
	features = feat_theano(model, training, ext_train, w_size, 1);

	return (model, scaler, exscaler, features);

#------------------------------------------------------------------------------
def train_cnn(training, cnn_train, cnn_ext, labels, train):
	print("Training f = 3...");
	(model3, scaler3, exscaler3, train3) = mut1dcnn_train_feat(
		cnn_train[train], cnn_ext[train], labels[train], 30, 3
	);
	ftrain = np.concatenate((training[train], train3), axis=1);

	print("Training f = 7...");
	(model7, scaler7, exscaler7, train7) = mut1dcnn_train_feat(
		cnn_train[train], cnn_ext[train], labels[train], 30, 7
	);
	ftrain = np.concatenate((ftrain, train7), axis=1);

	print("Training f = 11...");
	(model11, scaler11, exscaler11, train11) = mut1dcnn_train_feat(
		cnn_train[train], cnn_ext[train], labels[train], 30, 11
	);
	ftrain = np.concatenate((ftrain, train11), axis=1);

	models    = [model3, model7, model11];
	scalers   = [scaler3, scaler7, scaler11];
	exscalers = [exscaler3, exscaler7, exscaler11];

	return (ftrain, models, scalers, exscalers);

#------------------------------------------------------------------------------
def createxgb():
	params = {
		"learning_rate":    [0.05, 0.1, 0.3],
		"max_depth":        [2, 3, 4, 5],
		"min_child_weight": [1, 3, 5],
		"gamma":            [0],
		"scale_pos_weight": [0, 0.5, 1]
	};

	xgbmodel = XGBRegressor(n_estimators=5000, objective="reg:linear", scale_pos_weight=0.2);
	return xgbmodel, params;

#------------------------------------------------------------------------------
def fitmodel(training, labels):
	xgbmodel, params = createxgb();
	training, labels = shuffle(training, labels);

	[trainX, trainY, valX, valY] = split_data(training, labels, 0.2);

	(trainX, scaler) = standardize_data(trainX);
	valX = scaler.transform(valX);

	# not used...?
	fit_p = {
		"eval_set": [(valX, valY)],
		"eval_metric": "rmse",
		"verbose": False,
		"early_stopping_rounds":100
	};

	fit = xgbmodel.fit(
		trainX, trainY, eval_set=[(valX, valY)],
		eval_metric="rmse", verbose=False, early_stopping_rounds=200
	);

	return (xgbmodel, scaler);

#------------------------------------------------------------------------------
def split_data(training, labels, div):
	split_index = len(labels) - int(len(labels) * div);
	trainX      = training[0:split_index];
	testX       = training[split_index:len(labels)];
	trainY      = labels[0:split_index];
	testY       = labels[split_index:len(labels)];
	return (trainX, trainY, testX, testY);

#------------------------------------------------------------------------------
def mut1dcnn_test_feat(model, test, ext_test, scaler, exscaler, w_size):
	test = scaler.transform(test);
	for index, scale in enumerate(exscaler):
		ext_test[:, :, index] = scale.transform(ext_test[:, :, index]);
	ret = feat_theano(model, test, ext_test, w_size, 0);
	return ret;

#------------------------------------------------------------------------------
def test_cnn(training, cnn_train, cnn_ext, models, scaler, exscaler, test):
	test3  = mut1dcnn_test_feat(models[0], cnn_train[test], cnn_ext[test], scaler[0], exscaler[0], 30);
	ftest  = np.concatenate((training[test], test3), axis=1);

	test7  = mut1dcnn_test_feat(models[1], cnn_train[test], cnn_ext[test], scaler[1], exscaler[1], 30);
	ftest  = np.concatenate((ftest, test7), axis=1);

	test11 = mut1dcnn_test_feat(models[2], cnn_train[test], cnn_ext[test], scaler[2], exscaler[2], 30);
	ftest  = np.concatenate((ftest, test11), axis=1);

	return ftest;

#------------------------------------------------------------------------------
def testmodel(testX, testY, model, scaler):
	testX = scaler.transform(testX);
	preds = model.predict(testX);

	err = np.sqrt(mean_squared_error(testY, preds));
	print("Pearsons R: "+str(pearsonr(preds, testY)[0]));

	return (err, preds, testY);

#------------------------------------------------------------------------------
def runxgb(training, labels, features, cnn_train, cnn_ext):
	training, labels, cnn_train, cnn_ext = shuffle(training, labels, cnn_train, cnn_ext);
	binary = np.where(labels >= 0, 1, 0);
	k_fold = cross_validation.StratifiedKFold(binary, n_folds=10);

	fold     = 1;
	folderr  = [];
	feat_imp = [];
	tot_pred = [];
	tot_y    = [];

	for train, test in k_fold:
		(ftrain, models, scalecnn, exscaler) = train_cnn(training, cnn_train, cnn_ext, labels, train);

		t1 = ftrain.copy();
		l1 = labels[train].copy();

		print("Oversampling Beneficial Mutations...");
		#ros = RandomOverSampler()
		#ftrain1, ltrain = ros.fit_sample(t1, l1)
		#(ftrain1, ltrain) = random_os(t1, l1)
		#(t1, l1) = sample(t1, l1);
		#ltrain = labels[train]

		print("Training XGBoost model...");
		(xgbmodel, scaler) = fitmodel(t1, l1);
		ftest = test_cnn(training, cnn_train, cnn_ext, models, scalecnn, exscaler, test);
		(err, preds, testy) = testmodel(ftest, labels[test], xgbmodel, scaler);

		tot_pred.extend(preds);
		tot_y.extend(testy);
		folderr.append(err);

		# extract f importance for each fold and average it across all folds
		fimp = feat_importance(xgbmodel, features);
		if(fold == 1):
			feat_imp = fimp;
		else:
			feat_imp.add(fimp, fill_value=0);

		print("Fold "+str(fold)+": "+str(err));
		fold = fold + 1;

	return (folderr, feat_imp, tot_pred, tot_y);

#------------------------------------------------------------------------------
def extract_features(data):
	training = [[]]*len(data["Mutation"]);
	features = [];
	tindex = 0;

	pdbs = get_pdbs(data);
	(cdrdict, cdrch) = cdr_seq(pdbs);

	for pdb in pdbs:
		print(pdb);
		numex = data["#PDB"].count(pdb);
		featvec = [[]]*numex;
		feature = [];
		rows = np.where(np.array(data["#PDB"])==pdb);
		struct = parse_PDB(pdb);

		for mut in get_mut(pdb, data):
			(wild_type, mutant, ch, resid, chmut) = parse_mut(pdb, mut);
			if not (AAbrev(struct[0][ch][resid].get_resname()) == wild_type):
				print(chmut);

		# amino acid vector
		(vec1, f1) = aavector(pdb, data, rows);
		featvec = map(list.__add__, featvec, vec1);
		feature.extend(f1);

		# point mutation changes
		(vec2, f2, prop, flist) = aachange(pdb, data, rows);
		featvec = map(list.__add__, featvec, vec2);
		feature.extend(f2);

		# evolutionary features
		(vec3, f3) = get_evolfeatures(pdb, data, rows, struct);
		featvec = map(list.__add__, featvec, vec3);
		feature.extend(f3);

		# structual features
		(vec4, f4) = get_structfeatures(pdb, data, rows, struct, cdrdict);
		featvec = map(list.__add__, featvec, vec4);
		feature.extend(f4);

		training[tindex:tindex+numex] = featvec;
		tindex = tindex + numex;
		features = feature;

	return (np.array(training), features);

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------

g_data = read_data();

print("Feature extraction for XGBoost...");
g_labels = np.array(g_data["ddG(kcal/mol)"]).astype(float);

(g_training, g_features) = extract_features(g_data);
g_training = g_training.astype(float);

print("Getting sequence features for CNN...");
g_cnn_train = cnn_features(g_data, 30);

print("Getting structural features for CNN...");
g_cnn_ext = ext_features(g_data, 30);

print("Training model...");
(g_err, g_feat_imp, g_pred, g_y) = runxgb(g_training, g_labels, g_features, g_cnn_train, g_cnn_ext);

g_y = np.array(g_y);
g_pred = np.array(g_pred);

print("\nAverage error: "+str(np.mean(g_err)));
print("Pearsons R: "+str(pearsonr(g_pred, g_y)[0]));
print("P value: "+str(pearsonr(g_pred, g_y)[1]));

np.savetxt("yfile_out.txt", g_y, delimiter=",", fmt="%s");
np.savetxt("predfile_out.txt", g_pred, delimiter=",", fmt="%s");

# NOTE: save the feature importance
score_sum = sum(g_feat_imp.values);
g_feat_imp = g_feat_imp / float(score_sum);
g_feat_imp = g_feat_imp.order()[-15:];
g_feat_imp.to_csv("feat_imp.csv");
g_feat_imp = np.array(g_feat_imp);
np.savetxt("feat_imp.txt", g_feat_imp, delimiter=",", fmt="%s");

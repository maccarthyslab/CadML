from scipy.stats.stats import pearsonr
from Bio.PDB import *
from helperfunc import parse_PDB
import warnings
import numpy as np
import matplotlib.pyplot as plt

# ignore BioPython warnings for readability
warnings.filterwarnings('ignore')

def read_benedix():

    fname = '/Users/arjunsubramaniam/Desktop/MacCarthyLab/mlproject/benedix.csv'
    data = {}
    cols = ['#PDB', 'Mutation', 'ddG(kcal/mol)']

    for col in cols:
        data.update({col:[]})

    y = []
    pred = []
    with open(fname, 'r') as f:
        line = f.readline()
        for arr in line.split('"')[2:]:
            a = arr.split()
            if(len(a) >= 5 and not a[0] == 'Reverse'):
                data['Mutation'].append(a[3] + ':' + a[2])
                data['ddG(kcal/mol)'].append(-float(a[5]))
                data['#PDB'].append(a[1][0:4])
                pred.append(-float(a[4]))

    return data, pred

# data['ddG(kcal/mol'] is the ground truth, and pred is the predictions
# you can use this to generate the correlation scatter plot
data, pred = read_benedix()
